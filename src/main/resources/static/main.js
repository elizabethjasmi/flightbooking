
$(document).ready(function () {

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();
        fire_ajax_submit();

        });

});

function makeTable(container, data) {
       container.empty();
       if(data.length==0) {
              var noTickets = '<div class="panel panel-default">' +
                '<div class="panel-body">Sorry no tickets available on '+
                'the selected date </div>' +
              '</div>';

            return container.append(noTickets);
       }
       var table = $("<table/>").addClass('table');
       var header = '<thead>' +
                         '<tr>' +
                           '<th scope="col">Ref.No</th>'+
                           '<th scope="col">Fight Code</th>'+
                           '<th scope="col">Origin</th>'+
                           '<th scope="col">Destination</th>'+
                           '<th scope="col">Departure Date</th>'+
                           '<th scope="col">Travel Class</th>'+
                           '<th scope="col">Number of Passengers</th>'+
                           '<th scope="col">Total Fare</th>'+
                         '</tr>'+
                       '</thead><tbody/>';
        table.append(header);
       $.each(data, function(rowIndex, r) {
           var row = $("<tr/>");
           $.each(r, function(colIndex, c) {
               row.append($("<td/>").text(c));
           });
           table.append(row);
       });
       return container.append(table);
   }

function fire_ajax_submit() {

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/flights/search",
        data: {
                   source: $("#origin").val(),
                   destination: $("#destination").val(),
                   passengerCount: $("#passengerCount").val(),
                   departureDate: $("#departureDate").val(),
                   travelClass: $("#travelClass").val()
        },

        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

        var json =  makeTable($("#SearchResults"), data);

        console.log("SUCCESS : ", data);
        $("#btn-search").prop("disabled", false);

    },
    error: function (e) {

        var json = "<h4>Ajax Response</h4><pre>"
                        + e.responseText + "</pre>";
        $('#feedback').html(json);

        console.log("ERROR : ", e);
        $("#btn-search").prop("disabled", false);

    }
});

}