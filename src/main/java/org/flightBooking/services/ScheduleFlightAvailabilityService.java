package org.flightBooking.services;

import org.flightBooking.models.ScheduledFlight;
import org.flightBooking.repository.FlightScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ScheduleFlightAvailabilityService {

    private static final Logger LOGGER = Logger.getLogger(ScheduleFlightAvailabilityService.class.getName());
    private FlightScheduleRepository flightScheduleRepository;
    private LocalDateTime today = LocalDateTime.now();

    @Autowired
    public ScheduleFlightAvailabilityService(FlightScheduleRepository flightScheduleRepository) {
        this.flightScheduleRepository = flightScheduleRepository;

    }

    public List<ScheduledFlight> getAvailableFlights(String origin,
                                                           String destination
            ,final  Integer numberOfPassengers, LocalDateTime departureDate,
                                                           String travelClass) {

        List<ScheduledFlight> availableFlights =
                flightScheduleRepository.getAllFlights().values().stream()
                        .filter(i -> i.getRoute().getOrigin().equalsIgnoreCase(origin)
                                && i.getRoute().getDestination().equalsIgnoreCase(destination)
                                && checkAvailabilityOfSeats(i,numberOfPassengers, travelClass)
                                && areTheDatesEqual(i.getDepartureDate(), departureDate))
                        .collect(Collectors.toList());
        return availableFlights;

    }


    private boolean checkAvailabilityOfSeats(ScheduledFlight scheduledFlight,
                                             Integer numberOfPassengers, String travelClass) {
        int capacity = 0;
        float baseFare = 0;
        int daysBetweenBookingAndTravel =
                Math.toIntExact(ChronoUnit.DAYS.between(today, scheduledFlight.getDepartureDate()));
        switch (travelClass) {
            case "Economy Class":
                capacity = scheduledFlight.getFlight().getAircraftModel().getEconomyClassSeatingCapacity();
                break;
            case "Business Class":
                if (daysBetweenBookingAndTravel <= 28) {
                    capacity =
                            scheduledFlight.getFlight().getAircraftModel().getBusinessClassSeatingCapacity();
                }
                break;
            case "First Class":
                LOGGER.info("Days between booking and travel "+daysBetweenBookingAndTravel);
                if (daysBetweenBookingAndTravel <= 10) {
                    capacity = scheduledFlight.getFlight().getAircraftModel().getFirstClassSeatingCapacity();
                }
                break;
        }
        LOGGER.info("Capacity" + capacity);
        LOGGER.info("Number of Passengers" + numberOfPassengers);
        if (numberOfPassengers <= capacity) {
            LOGGER.info("Flight " + scheduledFlight.getScheduleCode() + "CAN " +
                    "accomodate" +
                    " " + numberOfPassengers + " passengers. Total ticket " +
                    "cost" + numberOfPassengers*baseFare);
            return true;

        } else {
            LOGGER.info("Flight " + scheduledFlight.getScheduleCode() +
                    "CAN NOT " +
                    "accomodate " + numberOfPassengers + " passengers");
            return false;
        }

    }

    private boolean areTheDatesEqual(LocalDateTime d1, LocalDateTime d2) {
        if ((d1.getDayOfYear() == d2.getDayOfYear()) && (d1.getYear() == d2.getYear())) {
            LOGGER.info("dates are SAME "+d1);
            return true;
        } else {
            LOGGER.info("Dates are different "+d1+" " +d2);
            return false;
        }
    }

}
