package org.flightBooking.services;

import com.google.common.base.Strings;
import org.flightBooking.models.ProFormaTicket;
import org.flightBooking.models.ScheduledFlight;
import org.flightBooking.repository.FlightScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.time.temporal.ChronoUnit;

@Service
public class FlightSearchService {

    LocalDateTime today = LocalDateTime.now();
    private static final Logger LOGGER = Logger.getLogger(FlightSearchService.class.getName());

    private FlightScheduleRepository flightScheduleRepository;
    private FlightFareEstimationService fareEstimationService;
    private ScheduleFlightAvailabilityService flightAvailabilityService;

    @Autowired
    public FlightSearchService(FlightScheduleRepository flightScheduleRepository) {
        this.flightScheduleRepository = flightScheduleRepository;
        this.fareEstimationService =
                new FlightFareEstimationService(this.flightScheduleRepository);
        this.flightAvailabilityService =
                new ScheduleFlightAvailabilityService(this.flightScheduleRepository);
    }


    public Collection<ScheduledFlight> getAllFlights() {

        Map<Integer, ScheduledFlight> flightDetails =
                flightScheduleRepository.getAllFlights();
        return flightDetails.values();
    }

    public Collection<ProFormaTicket> searchFlights(String origin,
                                                    String destination,
                                                    Integer numberOfPassengers,
                                                    LocalDateTime departureDate,
                                                    String travelClass) {
        if (Strings.isNullOrEmpty(origin) || Strings.isNullOrEmpty(destination)) {
            throw new RuntimeException("Origin and Destination cannot be Empty");
        }
        final LocalDateTime travelDate;
        if (Objects.isNull(departureDate)) {
            travelDate = LocalDateTime.now().plusDays(1);
        } else {
            travelDate = departureDate;
        }

        final Integer minimumNumberOfPassengers;
        if (Objects.isNull(numberOfPassengers)) {
            minimumNumberOfPassengers = 1;
        } else {
            minimumNumberOfPassengers = numberOfPassengers;
        }

        final String travelClassToSearch;
        if (Strings.isNullOrEmpty(travelClass)) {
            travelClassToSearch = "Economy Class";
        } else {
            travelClassToSearch = travelClass;
        }

        List<ScheduledFlight> availableFlights =
                flightAvailabilityService.getAvailableFlights(origin, destination,
                        minimumNumberOfPassengers, travelDate,
                        travelClassToSearch);

        return fareEstimationService.searchFlights(availableFlights, origin,
                destination,
                minimumNumberOfPassengers, travelDate,travelClassToSearch);
    }



    }



