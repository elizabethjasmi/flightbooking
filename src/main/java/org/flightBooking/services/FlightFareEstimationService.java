package org.flightBooking.services;

import com.google.common.base.Strings;
import org.flightBooking.models.ProFormaTicket;
import org.flightBooking.models.ScheduledFlight;
import org.flightBooking.repository.FlightScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class FlightFareEstimationService {
    private static final Logger LOGGER = Logger.getLogger(FlightFareEstimationService.class.getName());
    private LocalDateTime today = LocalDateTime.now();

    public FlightFareEstimationService(FlightScheduleRepository flightScheduleRepository) {
    }

    public Collection<ProFormaTicket> searchFlights(Collection<ScheduledFlight> availableFlights,
                                                    String origin,
                                                    String destination
            , Integer numberOfPassengers, LocalDateTime departureDate,
                                                    String travelClass) {

        LOGGER.info("Flights are:" + availableFlights.toString());

        LOGGER.info("Search parameters are:" + origin + " " + destination + " " + departureDate + " "
                + numberOfPassengers + " " + travelClass);

        return availableFlights.stream()
                .map((flight) -> issueProFormaTicket(flight,
                        numberOfPassengers,travelClass))
                .collect(Collectors.toList());

    }




    private ProFormaTicket issueProFormaTicket(ScheduledFlight scheduledFlight ,
                                              Integer numberOfPassengers,
                                              String travelClass) {
        ProFormaTicket proFormaTicket =
                createProFormaTicket(scheduledFlight, numberOfPassengers,
                        travelClass);

        switch (travelClass) {
            case "Economy Class":
                issueEconomyClassProFormaTicket(scheduledFlight,
                        numberOfPassengers,
                        proFormaTicket);
                break;

            case "Business Class":
                issueBusinessClassProFormaTicket(scheduledFlight, numberOfPassengers, proFormaTicket);
                break;

            case "First Class":
                issueFirstClassProFormaTicket(scheduledFlight, numberOfPassengers, proFormaTicket);
                break;
        }
        return proFormaTicket;
    }


    private void issueFirstClassProFormaTicket(ScheduledFlight scheduledFlight,
                                               Integer numberOfPassengers, ProFormaTicket proFormaTicket) {

        double surCharge = 0;
        LocalDateTime today = LocalDateTime.now();
        int daysBetweenBookingAndTravel =
                Math.toIntExact(ChronoUnit.DAYS.between(today,
                        scheduledFlight.getDepartureDate()));
        double baseFare =
                scheduledFlight.getFlight().getAircraftModel().getBasePriceForFirstClass();
        //for each day left with in that 10 day span  add 10% surcharge
        // to base price
        surCharge = baseFare * 0.1 * (10-daysBetweenBookingAndTravel);
        LOGGER.info("Basefare : "+ baseFare);
        LOGGER.info("Surcharge : "+ surCharge);
        scheduledFlight.setAvailableSeatsInFirstClass(scheduledFlight.getAvailableSeatsInFirstClass() - numberOfPassengers );
        proFormaTicket.setTotalFare(numberOfPassengers * (baseFare + surCharge));
    }

    private void issueBusinessClassProFormaTicket(ScheduledFlight scheduledFlight, Integer numberOfPassengers, ProFormaTicket proFormaTicket) {

        double surCharge = 0;
        double baseFare =
                scheduledFlight.getFlight().getAircraftModel().getBasePriceForBusinessClass();

        LOGGER.info("Day of week "+scheduledFlight.getDepartureDate().getDayOfWeek());
        if (scheduledFlight.getDepartureDate().getDayOfWeek().toString().equals("MONDAY") || scheduledFlight.getDepartureDate().getDayOfWeek().toString().equals("FRIDAY") ||
                scheduledFlight.getDepartureDate().getDayOfWeek().toString().equals("SUNDAY")) {
            surCharge = baseFare * 0.4;
            LOGGER.info("surcharge for Business class "+surCharge);
        }
        scheduledFlight.setAvailableSeatsInBusinessClass(scheduledFlight.getAvailableSeatsInBusinessClass()- numberOfPassengers);
        proFormaTicket.setTotalFare(numberOfPassengers * (baseFare + surCharge));
    }


    private void issueEconomyClassProFormaTicket(ScheduledFlight scheduledFlight
            , Integer numberOfPassengers, ProFormaTicket proFormaTicket) {
        double baseFare;
        double fillingRate;
        double surCharge;
        baseFare =
                scheduledFlight.getFlight().getAircraftModel().getBasePriceForEconomyClass();

        LOGGER.info("Available seats in economy class "+scheduledFlight.getAvailableSeatsInEconomyClass());
        LOGGER.info("capacity of economy class " +scheduledFlight.getFlight().getAircraftModel().getEconomyClassSeatingCapacity());
        fillingRate =
                ((double)scheduledFlight.getAvailableSeatsInEconomyClass() / (double)scheduledFlight.getFlight().getAircraftModel().getEconomyClassSeatingCapacity())*100;

        LOGGER.info("filling Rate "+fillingRate);

        if (fillingRate > 60) {
            surCharge = 0;
        } else if (fillingRate > 10) {
            surCharge = 0.3 * baseFare;
        } else {
            surCharge = 0.6 * baseFare;
        }
        LOGGER.info("surcharge for Economy class "+surCharge);
        scheduledFlight.setAvailableSeatsInEconomyClass(scheduledFlight.getAvailableSeatsInEconomyClass()- numberOfPassengers);
        LOGGER.info("Available seats in economy class "+scheduledFlight.getAvailableSeatsInEconomyClass());

        proFormaTicket.setTotalFare(numberOfPassengers * (baseFare + surCharge));

    }

    private ProFormaTicket createProFormaTicket(ScheduledFlight scheduledFlight, Integer numberOfPassengers,
                                                String travelClass) {
        ProFormaTicket proFormaTicket = new ProFormaTicket();
        Random random = new Random();
        proFormaTicket.setTicketId(
                random.ints(11111, 99999).limit(1).findFirst().getAsInt());
        proFormaTicket.setOrigin(scheduledFlight.getRoute().getOrigin());
        proFormaTicket.setDestination(scheduledFlight.getRoute().getDestination());
        proFormaTicket.setFlightScheduleCode(scheduledFlight.getScheduleCode());
        proFormaTicket.setTravelDate(scheduledFlight.getDepartureDate());
        proFormaTicket.setNumberOfPassengers(numberOfPassengers);
        proFormaTicket.setTravelClass(travelClass);
        return proFormaTicket;
    }
}
