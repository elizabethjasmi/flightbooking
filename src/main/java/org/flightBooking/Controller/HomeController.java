package org.flightBooking.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping({"/flights", "/"})
    public String index() {
        return "flight";
    }

}
