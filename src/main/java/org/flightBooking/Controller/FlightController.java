package org.flightBooking.Controller;

import org.flightBooking.models.ProFormaTicket;
import org.flightBooking.models.ScheduledFlight;
import org.flightBooking.services.FlightFareEstimationService;
import org.flightBooking.services.FlightSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.logging.Logger;

@CrossOrigin
@RestController
public class FlightController {
    FlightSearchService flightSearchService;
    private static final Logger LOGGER = Logger.getLogger(FlightController.class.getName());

    @Autowired
    FlightController(FlightSearchService flightSearchService) {
        this.flightSearchService =  flightSearchService;
    }

    @RequestMapping(value = "/flights/search")
    public Collection<ProFormaTicket> search(@RequestParam(value = "source",
            defaultValue = "HYD") String src,
                                             @RequestParam(value = "destination", defaultValue = "DEL") String dest,
                                             @RequestParam(value = "passengerCount", defaultValue = "1") Integer count,
                                             @RequestParam(value =
                                                      "departureDate"
                                                     , defaultValue =
                                                     "2019-08-30") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String dDate,
                                             @RequestParam(value = "travelClass", defaultValue = "Economy Class") String classType) {


        LOGGER.info("Date in String format " + dDate);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime travelDate = LocalDate.parse(dDate, formatter).atStartOfDay();

        LOGGER.info("Received the request");
        LOGGER.info("Date in Local date time format " + travelDate);
        return flightSearchService.searchFlights(src, dest, count
                , travelDate, classType);
    }


}
