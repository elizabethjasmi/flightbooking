package org.flightBooking.repository;
import java.time.LocalDateTime;
import java.util.*;

import org.flightBooking.models.AircraftModel;
import org.flightBooking.models.OperatingFlight;
import org.flightBooking.models.Route;
import org.springframework.stereotype.Repository;

import org.flightBooking.models.ScheduledFlight;


@Repository
public class FlightScheduleRepository {
    private Map<Integer, ScheduledFlight> flightsRepository = new HashMap<>();
    public FlightScheduleRepository(Map<Integer, ScheduledFlight> repository) {
        this.flightsRepository = repository;
        initialize();
    }

    private Map<Integer, ScheduledFlight> initialize() {

        LocalDateTime d1Morning = LocalDateTime.of(2019, 9, 10,6,35);
        LocalDateTime d2Evening = LocalDateTime.of(2019, 8, 30,19,15);
        LocalDateTime d2Morning = LocalDateTime.of(2019, 8, 30,5,40);
        LocalDateTime d3Morning = LocalDateTime.of(2019, 8, 31,7,5);
        LocalDateTime d4Morning = LocalDateTime.of(2019, 9, 9,6,5);
        LocalDateTime sepFirstMorning = LocalDateTime.of(2019, 9, 1,6,5);
        LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);

        AircraftModel boeing777 = new AircraftModel("777","BOEING 777","Boeing",
                195,35,8,6000,13000,20000);
        AircraftModel airbus319 = new AircraftModel("319","AIRBUS A319 V2",
                "Airbus", 144,0,0,4000,0,0);
        AircraftModel airbus321 = new AircraftModel("321","AIRBUS A321",
                "Airbus", 152,20,0,5000,10000,0);


        OperatingFlight indigoFlight1 = new OperatingFlight("ING001",
                "Indigo",
                airbus321);
        OperatingFlight spiceJetFlight1 = new OperatingFlight("SJ001","Spice" +
                " jet",
                airbus321);
        OperatingFlight spiceJetFlight2 = new OperatingFlight("SJ002","Spice" +
                " jet",
                airbus321);
        OperatingFlight indigoFlight2 = new OperatingFlight("ING002",
                "Indigo",
                airbus319);
        OperatingFlight jetAirwaysFlight1 = new OperatingFlight("JET001","Jet" +
                " Airways",
                airbus321);
        OperatingFlight indigoFlight3 = new OperatingFlight("ING003",
                "Indigo",
                boeing777);

        Route bangaloreToHyderabad = new Route("BLR",
                "HYD");
        Route hyderabadToBangalore = new Route("HYD","BLR");
        Route delhiToHyderabad = new Route("DEL",
                "HYD");
        Route hyderabadToDelhi = new Route("HYD","DEL");
        Route bangaloreToDelhi = new Route("BLR",
                "DEL");
        Route delhiToBangalore = new Route("DEL","BLR" );



        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
                indigoFlight1, bangaloreToHyderabad , "6E517",d1Morning);

        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight2, hyderabadToBangalore , "6E518",d1Morning);

        ScheduledFlight scheduledFlight3 = new ScheduledFlight(3333,
                indigoFlight3, bangaloreToDelhi , "6E533",d1Morning);

        ScheduledFlight scheduledFlight4 = new ScheduledFlight(1114,
                spiceJetFlight1, bangaloreToHyderabad , "SJ917",d1Morning);

        ScheduledFlight scheduledFlight5 = new ScheduledFlight(1116,
                spiceJetFlight1, hyderabadToBangalore , "6E518",d2Morning); //same spicejet flight is flying back the next day

        ScheduledFlight scheduledFlight6 = new ScheduledFlight(1118,
                jetAirwaysFlight1, hyderabadToBangalore , "JET230",d2Morning);

        ScheduledFlight scheduledFlight7 = new ScheduledFlight(1119,
                jetAirwaysFlight1, bangaloreToDelhi , "JET298",d2Evening);

        ScheduledFlight scheduledFlight8 = new ScheduledFlight(1108,
                jetAirwaysFlight1, delhiToHyderabad , "JET877",d3Morning);

        ScheduledFlight scheduledFlight9 = new ScheduledFlight(8989,
                spiceJetFlight2, bangaloreToDelhi , "SJ002",d1Morning);

        ScheduledFlight scheduledFlight11 = new ScheduledFlight(7003,
                indigoFlight3, bangaloreToDelhi , "ING399",d4Morning,12,0,0);

        ScheduledFlight scheduledFlight10 = new ScheduledFlight(1256,
                spiceJetFlight2, delhiToBangalore , "SJ872",d2Morning);

        ScheduledFlight scheduledFlight12 = new ScheduledFlight(6445,
                indigoFlight3, bangaloreToDelhi , "ING399",
                sepFirstMorning.plusDays(3),
                12,0,0);

        ScheduledFlight scheduledFlight13 = new ScheduledFlight(8955,
                indigoFlight2, bangaloreToDelhi , "ING559",
                sepFirstMorning.plusDays(3),
                25,0,0);

        ScheduledFlight scheduledFlight14 = new ScheduledFlight(3221,
                jetAirwaysFlight1, bangaloreToDelhi , "JET807",
                sepFirstMorning.plusDays(3),
                30,5,2);

        ScheduledFlight scheduledFlight15 = new ScheduledFlight(5644,
                spiceJetFlight1, bangaloreToDelhi , "SJ221",
                sepFirstMorning.plusDays(3),
                46,0,0);

        ScheduledFlight scheduledFlight16 = new ScheduledFlight(9903,
                indigoFlight3, hyderabadToBangalore , "ING889",d2Evening);



        flightsRepository.put(101, scheduledFlight1);
        flightsRepository.put(102, scheduledFlight2);
        flightsRepository.put(103, scheduledFlight3);
        flightsRepository.put(104, scheduledFlight4);
        flightsRepository.put(105, scheduledFlight5);
        flightsRepository.put(106, scheduledFlight6);
        flightsRepository.put(107, scheduledFlight7);
        flightsRepository.put(108, scheduledFlight8);
        flightsRepository.put(109, scheduledFlight9);
        flightsRepository.put(100, scheduledFlight10);
        flightsRepository.put(200, scheduledFlight11);
        flightsRepository.put(201, scheduledFlight12);
        flightsRepository.put(202, scheduledFlight13);
        flightsRepository.put(203, scheduledFlight14);
        flightsRepository.put(204, scheduledFlight15);
        flightsRepository.put(205,scheduledFlight16);

        return flightsRepository;
    }

    public Map<Integer, ScheduledFlight> getAllFlights() {
        return flightsRepository;
    }
}




