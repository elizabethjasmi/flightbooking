package org.flightBooking.models;



import java.util.Collection;
import java.util.List;

public class AjaxResponseBody {

    String msg;
    List<ProFormaTicket> proFormaTickets;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setResult(Collection<ProFormaTicket> flights) {
        for (ProFormaTicket proFormaTicket : this.proFormaTickets)
            flights.add(proFormaTicket);

    }

    public List<ProFormaTicket> getFlights() {
        return proFormaTickets;
    }
}



