package org.flightBooking.models;

import org.flightBooking.services.FlightSearchService;

import java.time.LocalDateTime;

import java.time.temporal.ChronoUnit;
import java.util.logging.Logger;
import java.util.Random;


public class ScheduledFlight {
    private int scheduleId;

    private OperatingFlight flight;
    private Route route;
    private String scheduleCode; //like 6E132
    private LocalDateTime departureDate;
    private int availableSeatsInEconomyClass;
    private int availableSeatsInBusinessClass;
    private int availableSeatsInFirstClass;

    private static final Logger LOGGER = Logger.getLogger(FlightSearchService.class.getName());


    public ScheduledFlight(int scheduleId, OperatingFlight flight,
                           Route route, String scheduleCode, LocalDateTime departureDate) {
        this.scheduleId = scheduleId;
        this.flight = flight;
        this.route = route;
        this.scheduleCode = scheduleCode;
        this.departureDate = departureDate;
        this.availableSeatsInEconomyClass =
                this.flight.getAircraftModel().getEconomyClassSeatingCapacity();
        this.availableSeatsInBusinessClass =
                this.flight.getAircraftModel().getBusinessClassSeatingCapacity();
        this.availableSeatsInFirstClass = this.flight.getAircraftModel().getFirstClassSeatingCapacity();

    }

    public ScheduledFlight(int scheduleId, OperatingFlight flight,
                           Route route, String scheduleCode,
                           LocalDateTime departureDate,
                           int availableSeatsInEconomyClass,
                           int availableSeatsInBusinessClass,
                           int availableSeatsInFirstClass) {
        this.scheduleId = scheduleId;
        this.flight = flight;
        this.route = route;
        this.scheduleCode = scheduleCode;
        this.departureDate = departureDate;
        this.availableSeatsInEconomyClass =
                availableSeatsInEconomyClass;
        this.availableSeatsInBusinessClass =
                availableSeatsInBusinessClass;
        this.availableSeatsInFirstClass = availableSeatsInFirstClass;

    }

    public int getScheduleId() {
        return scheduleId;
    }

    public OperatingFlight getFlight() {
        return flight;
    }

    public Route getRoute() {
        return route;
    }

    public String getScheduleCode() {
        return scheduleCode;
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public int getAvailableSeatsInEconomyClass() {
        return availableSeatsInEconomyClass;
    }

    public int getAvailableSeatsInBusinessClass() {
        return availableSeatsInBusinessClass;
    }

    public int getAvailableSeatsInFirstClass() {
        return availableSeatsInFirstClass;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public void setAvailableSeatsInEconomyClass(int availableSeatsInEconomyClass) {
        this.availableSeatsInEconomyClass = availableSeatsInEconomyClass;
    }

    public void setAvailableSeatsInBusinessClass(int availableSeatsInBusinessClass) {
        this.availableSeatsInBusinessClass = availableSeatsInBusinessClass;
    }

    public void setAvailableSeatsInFirstClass(int availableSeatsInFirstClass) {
        this.availableSeatsInFirstClass = availableSeatsInFirstClass;
    }


}
