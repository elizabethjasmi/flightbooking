package org.flightBooking.models;

public class AircraftModel {
    private String aircraftModelId;
    private String aircraftModelName;
    private String manufacturerName;
    private int economyClassSeatingCapacity;
    private int businessClassSeatingCapacity;
    private int firstClassSeatingCapacity;
    private float basePriceForEconomyClass;
    private float basePriceForBusinessClass;
    private float basePriceForFirstClass;

    public AircraftModel(String aircraftModelId, String aircraftModelName, String manufacturerName, int economyClassSeatingCapacity, int businessClassSeatingCapacity, int firstClassSeatingCapacity, float basePriceForEconomyClass, float basePriceForBusinessClass, float basePriceForFirstClass) {
        this.aircraftModelId = aircraftModelId;
        this.aircraftModelName = aircraftModelName;
        this.manufacturerName = manufacturerName;
        this.economyClassSeatingCapacity = economyClassSeatingCapacity;
        this.businessClassSeatingCapacity = businessClassSeatingCapacity;
        this.firstClassSeatingCapacity = firstClassSeatingCapacity;
        this.basePriceForEconomyClass = basePriceForEconomyClass;
        this.basePriceForBusinessClass = basePriceForBusinessClass;
        this.basePriceForFirstClass = basePriceForFirstClass;
    }

    public String getAircraftModelId() {
        return aircraftModelId;
    }

    public String getAircraftModelName() {
        return aircraftModelName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public int getEconomyClassSeatingCapacity() {
        return economyClassSeatingCapacity;
    }

    public int getBusinessClassSeatingCapacity() {
        return businessClassSeatingCapacity;
    }

    public int getFirstClassSeatingCapacity() {
        return firstClassSeatingCapacity;
    }

    public float getBasePriceForEconomyClass() {
        return basePriceForEconomyClass;
    }

    public float getBasePriceForBusinessClass() {
        return basePriceForBusinessClass;
    }

    public float getBasePriceForFirstClass() {
        return basePriceForFirstClass;
    }
}

