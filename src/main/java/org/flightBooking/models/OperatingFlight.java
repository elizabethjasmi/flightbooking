package org.flightBooking.models;

public class OperatingFlight {
    private String flightId;
    private String operatingCompany;
    private AircraftModel aircraftModel;

    public OperatingFlight(String flightId, String operatingCompany, AircraftModel aircraftModel) {
        this.flightId = flightId;
        this.operatingCompany = operatingCompany;
        this.aircraftModel = aircraftModel;
    }

    public String getFlightId() {
        return flightId;
    }

    public String getOperatingCompany() {
        return operatingCompany;
    }

    public AircraftModel getAircraftModel() {
        return aircraftModel;
    }
}
