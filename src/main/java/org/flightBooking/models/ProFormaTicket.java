package org.flightBooking.models;

import java.time.LocalDateTime;

public class ProFormaTicket {

    private int ticketId;
    private String flightScheduleCode;
    private  String origin;
    private String destination;
    private LocalDateTime travelDate;
    private String travelClass;
    private int numberOfPassengers;
    private double totalFare;

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public void setFlightScheduleCode(String flightScheduleCode) {
        this.flightScheduleCode = flightScheduleCode;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setTravelDate(LocalDateTime travelDate) {
        this.travelDate = travelDate;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public int getTicketId() {
        return ticketId;
    }

    public String getFlightScheduleCode() {
        return flightScheduleCode;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public LocalDateTime getTravelDate() {
        return travelDate;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public double getTotalFare() {
        return totalFare;
    }
}
