package org.flightBooking.models;

public enum TravelClass {
    ECONOMY_CLASS("Economy Class"), BUSINESS_CLASS("Business Class"), FIRST_CLASS("First Class");
    private String description;
    private TravelClass(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        return description;
    }
}

