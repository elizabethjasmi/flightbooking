package org.flightBooking.models;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

public class SearchCriteria {

  //  @NotBlank(message = "username can't empty!")
    //String username;
    String origin;
    String destination;
    Integer numberOfPassengers;
    Date departureDate;
    String travelClass;

    public Date getDepartureDate() {
        return departureDate;
    }

    public String getTravelClass() {
        return travelClass;
    }


    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }


    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public Integer getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setNumberOfPassengers(Integer numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }
}
