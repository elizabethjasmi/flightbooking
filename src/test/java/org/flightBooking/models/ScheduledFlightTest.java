package org.flightBooking.models;

import org.flightBooking.repository.FlightScheduleRepository;
import org.flightBooking.services.FlightFareEstimationService;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class ScheduledFlightTest {


//    ScheduledFlight getDefaultBoeingSameDayFlightForTest(){
//        AircraftModel boeing777 = new AircraftModel("777","BOEING 777","Boeing",
//                195,35,8,6000,13000,20000);
//
//        OperatingFlight indigoFlight1 = new OperatingFlight("ING001",
//                "Indigo", boeing777);
//        Route hyderabadToBangalore = new Route("HYD",
//                "BLR");
//        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
//                indigoFlight1, hyderabadToBangalore,
//                "ING067", LocalDateTime.now());
//        return scheduledFlight1;
//    }
//
//    ScheduledFlight getDefaultBoeingAfter5DaysFlightForTest(){
//        AircraftModel boeing777 = new AircraftModel("777","BOEING 777","Boeing",
//                195,35,8,6000,13000,20000);
//
//        OperatingFlight indigoFlight1 = new OperatingFlight("ING001",
//                "Indigo", boeing777);
//        Route hyderabadToBangalore = new Route("HYD",
//                "BLR");
//        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
//                indigoFlight1, hyderabadToBangalore,
//                "ING067", LocalDateTime.now().plusDays(5));
//        return scheduledFlight1;
//    }
//
//    ScheduledFlight getDefaultAirBus321SameDayFlightForTest(){
//        AircraftModel airbus321 = new AircraftModel("321", "AIRBUS A321",
//                "Airbus", 195, 20,
//                0, 6000, 10000, 0);
//
//        OperatingFlight indigoFlight1 = new OperatingFlight("ING001",
//                "Indigo", airbus321);
//        Route hyderabadToBangalore = new Route("HYD",
//                "BLR");
//        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
//                indigoFlight1, hyderabadToBangalore,
//                "ING067", LocalDateTime.now());
//        return scheduledFlight1;
//    }

//    @Test
//    void issueProFormaTicketForEconomyClass() {
//        // 195 seats, till 100, it is 6000,
//        ScheduledFlight scheduledFlight =
//                getDefaultAirBus321SameDayFlightForTest();
//
//        FlightScheduleRepository flightScheduleRepository;
//        Collection<ScheduledFlight> scheduledFlights =
//               flightScheduleRepository.getAllFlights().values();
//        FlightFareEstimationService flightFareEstimationService =
//               new FlightFareEstimationService(flightScheduleRepository);
//        ProFormaTicket ticket =
//                issueProFormaTicket(scheduledFlight,100,
//                TravelClass.ECONOMY_CLASS.toString());
//        assertEquals(ticket.getTotalFare(), 6000*100);
//
//        ProFormaTicket ticket2 = scheduledFlight.issueProFormaTicket(2,
//                TravelClass.ECONOMY_CLASS.toString());
//        assertEquals(ticket2.getTotalFare(), 15600);
//    }
//
//
//    @Test
//    void issueProFormaTicketForFirstClassIfBookedForNextDay() {
//        ScheduledFlight scheduledFlight =
//                getDefaultBoeingSameDayFlightForTest();
//        ProFormaTicket ticket = scheduledFlight.issueProFormaTicket(1,
//                TravelClass.FIRST_CLASS.toString());
//        assertEquals(ticket.getTotalFare(), (10*0.1*20000) +20000);
//    }
//
//    @Test
//    void issueProFormaTicketForFirstClassIfBooked5DaysDefore() {
//        // 195 seats, till 100, it is 6000,
//        ScheduledFlight scheduledFlight =
//                getDefaultBoeingAfter5DaysFlightForTest();
//        ProFormaTicket ticket = scheduledFlight.issueProFormaTicket(1,
//                TravelClass.FIRST_CLASS.toString());
//        assertEquals(ticket.getTotalFare(), (6*0.1*20000) +20000);
//    }
}