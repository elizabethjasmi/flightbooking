package org.flightBooking.services;

import org.flightBooking.models.*;
import org.flightBooking.repository.FlightScheduleRepository;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FlightSearchServiceTest {

    LocalDateTime todayCurrentTime = LocalDateTime.now();
    LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);
    LocalDateTime d1Morning = LocalDateTime.of(2019, 9, 10,6,35);
    LocalDateTime d2Evening = LocalDateTime.of(2019, 8, 30,19,15);
    LocalDateTime d2Morning = LocalDateTime.of(2019, 8, 30,5,40);
    LocalDateTime d3Morning = LocalDateTime.of(2019, 8, 31,7,5);
    LocalDateTime d4Morning = LocalDateTime.of(2019, 9, 9,6,5);
    LocalDateTime sepFirstMorning = LocalDateTime.of(2019, 9, 1,6,5);


    AircraftModel boeing777 = new AircraftModel("777","BOEING 777","Boeing",
            195,35,8,6000,13000,20000);
    AircraftModel airbus319 = new AircraftModel("319","AIRBUS A319 V2",
            "Airbus", 144,0,0,4000,0,0);
    AircraftModel airbus321 = new AircraftModel("321","AIRBUS A321",
            "Airbus", 152,20,0,5000,10000,0);

    OperatingFlight jetAirwaysFlight1 = new OperatingFlight("JET001","Jet" +
            " Airways",
            airbus321);
    OperatingFlight indigoFlight3 = new OperatingFlight("ING003",
            "Indigo",
            boeing777);


    Route bangaloreToHyderabad = new Route("BLR",
            "HYD");
    Route hyderabadToBangalore = new Route("HYD","BLR");
    Route delhiToHyderabad = new Route("DEL",
            "HYD");
    Route hyderabadToDelhi = new Route("HYD","DEL");
    Route bangaloreToDelhi = new Route("BLR",
            "DEL");
    Route delhiToBangalore = new Route("DEL","BLR" );

    @Test
    public void shouldReturnAllTheFlights() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();
        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
                indigoFlight3, bangaloreToHyderabad , "6E517",d1Morning);
        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1122,
                jetAirwaysFlight1, bangaloreToHyderabad , "JET989", tomorrow);
        scheduledFlights.put(111, scheduledFlight1);
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);

        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);

        Collection<ScheduledFlight> allFlights = flightSearchService.getAllFlights();
        assertEquals(2, allFlights.size());
    }


    @Test
    public void shouldReturnTwoWhen2FlightsInThatRouteAreAvailable() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();

        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
                indigoFlight3, bangaloreToHyderabad , "6E517",d1Morning);
        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1122,
                jetAirwaysFlight1, bangaloreToHyderabad , "JET989", d1Morning);
        scheduledFlights.put(111, scheduledFlight1);
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);

        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);

        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", 3, d1Morning,
                        "Economy Class");
        assertEquals(2, proFormaTickets.size());
    }

    @Test
    public void shouldReturnZeroWhenNoFlightsInThatRouteAreAvailable() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();

        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
                indigoFlight3, bangaloreToHyderabad , "6E517",d1Morning);
        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1122,
                jetAirwaysFlight1, bangaloreToHyderabad , "JET989", d1Morning);
        scheduledFlights.put(111, scheduledFlight1);
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);

        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);

        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BOM", "HYD", 3, d1Morning,
                        "Economy Class");
        assertEquals(0, proFormaTickets.size());
    }

    @Test
    public void shouldReturnNextDayFlightsIfNoDateIsGiven() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();

        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
                indigoFlight3, bangaloreToHyderabad , "6E517",tomorrow);
        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1122,
                jetAirwaysFlight1, bangaloreToHyderabad , "JET989", todayCurrentTime);
        scheduledFlights.put(111, scheduledFlight1);
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);

        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);

        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", 3, null,
                        "Economy Class");
        assertEquals(1, proFormaTickets.size());
    }

    @Test
    public void shouldTakeOneAsDefautIfNumberOfPassengersIsNotGiven() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();

        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
                indigoFlight3, bangaloreToHyderabad , "6E517",d1Morning);
        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1122,
                jetAirwaysFlight1, bangaloreToHyderabad , "JET989", d1Morning);
        scheduledFlights.put(111, scheduledFlight1);
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);

        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);

        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        d1Morning,"Economy Class");
        assertEquals(2, proFormaTickets.size());
    }

    @Test
    public void shouldTakeTravelClassAsEconomyWhenNotGiven() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();

        ScheduledFlight scheduledFlight1 = new ScheduledFlight(1111,
                indigoFlight3, bangaloreToHyderabad , "6E517",d1Morning);
        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1122,
                jetAirwaysFlight1, bangaloreToHyderabad , "JET989", d1Morning);
        scheduledFlights.put(111, scheduledFlight1);
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);

        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);

        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        d1Morning, null);
        assertEquals(2, proFormaTickets.size());

    }


    @Test
    public void shouldReturn60percentExtraEconomyPriceWhenOneFlightHasEconomySeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                todayCurrentTime.plusDays(1),12,0,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                todayCurrentTime.plusDays(1), "Economy Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),9600);

    }

    @Test
    public void shouldReturn60percentExtraEconomyPriceWhenOneFlightHasLast10PercentEconomySeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                todayCurrentTime.plusDays(1),12,0,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        todayCurrentTime.plusDays(1), "Economy Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),9600);

    }

    @Test
    public void shouldReturn30percentExtraEconomyPriceWhenOneFlightHasSecondLast50PercentEconomySeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                todayCurrentTime.plusDays(1),90,0,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        todayCurrentTime.plusDays(1), "Economy Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),7800);

    }

    @Test
    public void shouldReturnBaseFareEconomyPriceWhenOneFlightLessThan40PercentFilledUpEconomySeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                todayCurrentTime.plusDays(1),140,0,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        todayCurrentTime.plusDays(1), "Economy Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),6000);

    }

    @Test
    public void shouldReturn40PercentExtraBusinessClassPriceWhenOneFlightDepartingOnSundayHasBusinessClassSeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                sepFirstMorning,0,25,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        sepFirstMorning, "Business Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),18200);
    }

    @Test
    public void shouldReturn40PercentExtraBusinessClassPriceWhenOneFlightDepartingOnMondayHasBusinessClassSeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                sepFirstMorning.plusDays(1),0,25,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        sepFirstMorning.plusDays(1), "Business Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),18200);
    }

    @Test
    public void shouldReturn40PercentExtraBusinessClassPriceWhenOneFlightDepartingOnFridayHasBusinessClassSeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                sepFirstMorning.plusDays(5),0,25,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        sepFirstMorning.plusDays(5), "Business Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),18200);
    }

    @Test
    public void shouldReturnBaseBusinessClassPriceWhenOneFlightDepartingOnTuesDayHasBusinessClassSeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                sepFirstMorning.plusDays(2),0,25,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        sepFirstMorning.plusDays(2), "Business Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),13000);
    }

    @Test
    public void shouldReturnBaseBusinessClassPriceWhenOneFlightDepartingOnSaturdayHasBusinessClassSeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                sepFirstMorning.minusDays(1),0,25,0); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        sepFirstMorning.minusDays(1), "Business Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),13000);
    }

    @Test
    public void shouldReturn100PercentExtraPriceWhenOneFlightDepartingNextDayHasFirstClassSeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                tomorrow,0,0,1); //scheduled for tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        tomorrow, "First Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),40000);
    }
    @Test
    public void shouldReturn90PercentExtraPriceWhenOneFlightDepartingDayAfterTomorrowHasFirstClassSeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                todayCurrentTime.plusDays(2),0,0,1); //scheduled for day after
        // tomorrow
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        todayCurrentTime.plusDays(2), "First Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),38000);
    }

    @Test
    public void shouldReturnBaseFareForBusinessClassWhenOneFlightscheduled11DaysAfterTodaysHasFirstClassSeats() {
        Map<Integer, ScheduledFlight> scheduledFlights = new HashMap<>();


        ScheduledFlight scheduledFlight2 = new ScheduledFlight(1112,
                indigoFlight3, bangaloreToHyderabad,
                "ING678",
                todayCurrentTime.plusDays(11),0,0,1);
        scheduledFlights.put(222, scheduledFlight2);

        FlightScheduleRepository flightScheduleRepository =
                mock(FlightScheduleRepository.class);
        when(flightScheduleRepository.getAllFlights()).thenReturn(scheduledFlights);
        FlightSearchService flightSearchService =
                new FlightSearchService(flightScheduleRepository);
        Collection<ProFormaTicket> proFormaTickets =
                flightSearchService.searchFlights("BLR", "HYD", null,
                        todayCurrentTime.plusDays(11), "First Class");
        assertEquals(proFormaTickets.iterator().next().getTotalFare(),20000);
    }

}