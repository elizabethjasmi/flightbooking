package org.flightBooking.services;

import org.junit.jupiter.api.Test;

import static junit.framework.TestCase.assertTrue;

public class TestFunctions {

    @Test
    public void testPercentage(){
        int availableFlightsInEconomy = 12;
        int capacity = 195;

        double fillingPercentage =
                ((double) availableFlightsInEconomy / (double) capacity);
        System.out.println(fillingPercentage);
        assertTrue(fillingPercentage > 0.06);

    }
}
